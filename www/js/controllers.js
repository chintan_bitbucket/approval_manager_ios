
var app = angular.module('starter.controllers', ['ionic', 'ngCordova', 'starter.services', 'starter.filters']);
app.controller('AppCtrl', function ($scope, $ionicModal, $timeout, $state, $rootScope, $window, LocalStoreData) {

    // With the new view caching in Ionic, Controllers are only called
    // when they are recreated or on app start, instead of every page change.
    // To listen for when this page is active (for example, to refresh data),
    // listen for the $ionicView.enter event:
    //$scope.$on('$ionicView.enter', function(e) {
    //});
    $rootScope.show_order_menu = false;
    $rootScope.show_order_button = false;
    $scope.toggleOrderMenu = function () {
        console.log("In toggleOrderMenu");
        $rootScope.show_order_menu = !$rootScope.show_order_menu;
    }
    
    $scope.logOut = function () {
        var logindata = LocalStoreData.getLocalData();
        $rootScope.formdata.login_method = logindata.Login.login_method;
        $rootScope.formdata.user_name = logindata.Login.user_name;
        $rootScope.formdata.portal_url = logindata.Login.portal_url;
        $rootScope.formdata.is_remember = logindata.Login.is_remember;
        $rootScope.formdata.password = "";

        logindata.Sort_criteria = "";
        logindata.Order_by = "";
        $rootScope.LocalStore.setItem('escribe', JSON.stringify(logindata));
        if (!logindata.Login.is_remember) {
            $rootScope.LocalStore.setItem('escribe', "{}");
            $rootScope.formdata = {portal_url: "", user_name: "", password: "", is_remember: false, login_method: "", auth_token: ""};
        }
        console.log("In logout logindata", $rootScope.LocalStore.getItem('escribe'));
        $state.go('login');
        /*
         $rootScope.LocalStore.setItem('escribe',JSON.stringify({"Login":""}));
         $state.go('login');
         */
    }


}).controller("LoginCtrl", function ($scope, $ionicSideMenuDelegate, LoginService, $state, $window, $rootScope, $ionicLoading,$cordovaNetwork) {

    //$scope.formdata = {portal_url:"",user_name:"",password:"",is_remember:false,login_method:"",auth_token:""};
    if ($rootScope.formdata) {
        $scope.formdata = $rootScope.formdata;
    } else {
        $scope.formdata = {portal_url: "", user_name: "", password: "", is_remember: false, login_method: "", auth_token: ""};
    }

    $scope.login_failed_msg = "";
    $scope.showLoading = function () {
        $ionicLoading.show({
            template: 'Loading...'
        }).then(function () {
            //console.log("The loading indicator is now displayed");
        });
    };
    $scope.hideLoading = function () {
        $ionicLoading.hide();
        /*$ionicLoading.hide().then(function(){
         
         });*/
    };
    $scope.doLogin = function () {
        if ($cordovaNetwork.isOffline()) {
            console.log("device offline");
            $rootScope.showToast($rootScope.getDisplayMessage(501));
            return;
        }
        $scope.showLoading();
        LoginService.loginUser($scope.formdata).success(function (data, status, headers, config) {

            var headersObj = headers();
            var auth_token = headersObj['auth-token'];

            $scope.hideLoading();
            //data.d
            // data = true/false
            if (data) {
                var logindata = {portal_url: $scope.formdata.portal_url, user_name: $scope.formdata.user_name, is_remember: $scope.formdata.is_remember, login_method: $scope.formdata.login_method};
                $scope.formdata.auth_token = auth_token;
                $rootScope.LocalStore.setItem("escribe", JSON.stringify({"Login": $scope.formdata}));
                $state.go('app.approvals');
            } else {
                $scope.login_failed_msg = "Invalid login details.";
                $rootScope.LocalStore.setItem("escribe", "{}");

            }
        }).error(function (data) {
            $scope.hideLoading();
            $scope.login_failed_msg = "Invalid login details.";
        });

    }

});
