//var app = angular.module('starter.controllers', []);

  
app.controller("ApprovalsCtrl",[ '$scope', 'GetServiceData','$rootScope','orderObjectByFilter','$ionicLoading',function($scope,GetServiceData,$rootScope,orderObjectByFilter,$ionicLoading){

  console.log("In ApprovalsCtrl Ctrl");
  
  
  $scope.getApprovalMeetingDate = function(meeting_id){
      
      for(var item in $scope.Meetings){
          if($scope.Meetings[item].Id == meeting_id){
              return $scope.Meetings[item].Title;
          }
      }
      
  }
        $scope.Tasks = [];
        $scope.MeetingTypes=[
        ];
        $scope.Meetings=[
                            
                    ];
        $scope.approvals = [
                    
                    ]



        
  
  $scope.isApprovalShown = function(approval) {
    return approval.show;
  }; 
  $scope.toggleApprovalItem = function(approval) {
      approval.show = !approval.show;
  };  
  $scope.comments = [];  
  $scope.nocomment = [];
  $scope.approveApproval = function(approval,ind){


    console.log("approval",approval);
      console.log("comment ",$scope.comments[ind]);
      if(!$scope.comments[ind]){
          $scope.comments[ind] = "";
      }
      console.log("comment ",$scope.comments[ind]);
        
        $scope.showLoading("Loading...");
      GetServiceData.Approve(approval.TaskId,true,$scope.comments[ind]).success(function(data, status, headers, config){
         $scope.hideLoading(); 
      }).error(function(){
          $scope.hideLoading();
      })
      
      
  }  
  $scope.rejectApproval = function(approval,ind){
      console.log("comment ",$scope.comments[ind]);
      if(!$scope.comments[ind]){
        $scope.comments[ind] = "";
           
      }
       console.log("comment ",$scope.comments[ind]);
      /*console.log("comment ",$scope.comments[ind]);
      if(!$scope.comments[ind] || $scope.comments[ind] == ""){
          $scope.nocomment[ind] = true;
           
      }
      else{
      GetServiceData.Approve(approval.TaskId,false,$scope.comments[ind]).success(function(data, status, headers, config){
    
      }).error(function(){
    
      })
      }
      */
      $scope.showLoading("Loading...");
      GetServiceData.Approve(approval.TaskId,false,$scope.comments[ind]).success(function(data, status, headers, config){
          $scope.hideLoading();
      }).error(function(){
          $scope.hideLoading();
    
      })


  }    
  $scope.setApprovalStatusClass   = function(approval){

        if(approval.ApprovalStatus == "Approval Started" || (approval.Approvers!= null && approval.Approvers.length > 0)){
            
            return "green";
        }
        //console.log("approval.Meeting.SubmissionDeadlinePassed"+approval.Meeting.SubmissionDeadlinePassed);
        if(approval.Meeting.SubmissionDeadlinePassed!=undefined && approval.Meeting.SubmissionDeadlinePassed){
            return "red";
        }
        //console.log("approval.Meeting.SubmissionReminderPassed "+approval.Meeting.SubmissionReminderPassed );
        if((approval.Meeting.SubmissionReminderPassed !=undefined && approval.Meeting.SubmissionReminderPassed) || approval.ApprovalStatus=="Draft" ){
            return "yellow";
        }
        /*switch(approval.ApprovalStatus){
            case "Approval Started":
                return "green";
            break;
            case "Draft":
                if(approval.Meeting.SubmissionDeadlinePassed)
                return "yellow";
            break;
            case "rejected":
                return "red";
            break;
                    
        }*/

  }
  
  $scope.showDocs = function(docs){
    docs.is_open = !docs.is_open;
  }
  $scope.showDocList = function(doc){
      return doc.is_open;
  }
  
  $scope.showComment = function(approval_comment){
      return approval_comment.show_comment;
  }
  $scope.toggleComment = function(approval_comment){
      approval_comment.show_comment = !approval_comment.show_comment;
  }
  $scope.logindata = JSON.parse($rootScope.LocalStore.getItem("escribe"));
  console.log("Logindata",$scope.logindata);
  function findElement(arr, propName, propValue) {
      
      
  var res_arr = [];
  for (var i=0; i < arr.length; i++){
    if (arr[i][propName] == propValue)
      res_arr.push(arr[i]);
    }
    // will return undefined if not found; you could return a default instead
    return res_arr;
   }
  $scope.prepareApprovalsData = function(approval_response){
      
      
      //$scope.approvals    = findElement(approval_response.Approvals,"ApprovalStatus","Approval Started");
       $scope.approvals    = approval_response.Approvals;
      $scope.Meetings     = approval_response.Meetings;
      $scope.MeetingTypes = approval_response.MeetingTypes;
      
      
      if($scope.approvals.length > 0){
          
       $rootScope.show_order_button = true;
      for(var approval in $scope.approvals){
          //var meeting_type_e =  
          for(var meeting in $scope.Meetings){
              $scope.approvals[approval]['Meeting'] = {};
              if($scope.approvals[approval].MeetingId == $scope.Meetings[meeting].Id){
                 
                var expr = /\/Date\((.*?)\)\//gi;
                var stamp = expr.exec($scope.Meetings[meeting].Date);
                var timestamp = stamp[1].split("-", 1);
                $scope.approvals[approval]['Meeting'] =  $scope.Meetings[meeting];
                $scope.approvals[approval]['Meeting'].Date = timestamp[0];
                 break;
              }
          }
          
          for (var i in $scope.MeetingTypes){
              $scope.approvals[approval]['MeetingType']= {};
              if($scope.approvals[approval].MeetingTypeId == $scope.MeetingTypes[i].Id){
                  $scope.approvals[approval]['MeetingType'] = $scope.MeetingTypes[i];
                  $scope.approvals[approval]['MeetingType_Title'] = $scope.MeetingTypes[i].Title.E+"_"+$scope.approvals[approval].Title;
                  break;
              }
              
          }
          
      }
      }
      else{
          $rootScope.show_order_button = false;
      }
      
     
  }
  $scope.toggle_approval_title = false;
  $scope.toggle_meeting_type = false;
  $scope.toggle_meeting_date = false;
  $scope.sortBy = function(what){
      
      switch(what){
          case 'approval_title':
            $scope.toggle_approval_title = !$scope.toggle_approval_title;
            $scope.temp_data = orderObjectByFilter($scope.approvals,'Title',$scope.toggle_approval_title); 
            $scope.approvals = $scope.temp_data;
            
          break;
          
          case 'meeting_type':
            $scope.toggle_meeting_type = !$scope.toggle_meeting_type;
            $scope.temp_data = orderObjectByFilter($scope.approvals,'MeetingType_Title',$scope.toggle_meeting_type); 
            
            $scope.approvals = $scope.temp_data;
          break;
          
          case 'meeting_date':
             $scope.toggle_meeting_date = !$scope.toggle_meeting_date;
             $scope.temp_data = orderObjectByFilter($scope.approvals,'Meeting.Date',$scope.toggle_meeting_date); 
             $scope.approvals = $scope.temp_data;
              
      }
  }
  $scope.showLoading = function(str_label) {

        if(!str_label || str_label==""){
          str_label = 'Loading approvals...';
        }

        $ionicLoading.show({
          template: str_label
        }).then(function(){
           //console.log("The loading indicator is now displayed");
        });
  };
   $scope.hideLoading = function(){
       $ionicLoading.hide();
    /*$ionicLoading.hide().then(function(){
       
    });*/
    };
  $scope.showLoading();  
  GetServiceData.getData($scope.logindata.Login).success(function(data, status, headers, config){
            
            $scope.hideLoading();
            $scope.prepareApprovalsData(data);
            
            
  }).error(function(data, status, headers, config) {
      
      console.log("Error"+JSON.stringify(data));
      $scope.hideLoading();
  });
    $scope.doRefresh = function(){
    GetServiceData.getData($scope.logindata.Login).success(function(data, status, headers, config){
            
            $scope.prepareApprovalsData(data);
            $scope.$broadcast('scroll.refreshComplete');
            
  }).error(function(data, status, headers, config) {
      
      console.log("Error"+JSON.stringify(data));
      $scope.$broadcast('scroll.refreshComplete');
  });
  }
  $scope.inAppOpen = function(doc_url) {
    try {
        
        window.open(doc_url, '_system');
    } catch (err) {
        console.log(err);
    }
    }
}]);

