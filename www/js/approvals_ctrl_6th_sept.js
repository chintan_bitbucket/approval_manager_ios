//var app = angular.module('starter.controllers', []);


app.controller("ApprovalsCtrl", ['$scope', 'GetServiceData', '$rootScope', 'orderObjectByFilter', '$ionicLoading', '$timeout', '$ionicScrollDelegate', '$cordovaFileTransfer', 'LocalStoreData', '$cordovaFileOpener2', '$ionicModal', '$cordovaNetwork', '$cordovaInAppBrowser',
    function ($scope, GetServiceData, $rootScope, orderObjectByFilter, $ionicLoading, $timeout, $ionicScrollDelegate, $cordovaFileTransfer, LocalStoreData, $cordovaFileOpener2, $ionicModal, $cordovaNetwork, $cordovaInAppBrowser) {

        console.log("In ApprovalsCtrl Ctrl");


        $scope.getApprovalMeetingDate = function (meeting_id) {

            for (var item in $scope.Meetings) {
                if ($scope.Meetings[item].Id == meeting_id) {
                    return $scope.Meetings[item].Title;
                }
            }

        }
        $scope.Tasks = [];
        $scope.MeetingTypes = [];
        $scope.Meetings = [];
        $scope.approvals = [];

        $scope.no_approval_visible = false;
        $scope.app_rej_visible = false;
        $scope.app_rej_msg = "";


        $scope.isApprovalShown = function (approval) {
            return approval.show;
        };
        $scope.toggleApprovalItem = function (approval) {
            approval.show = !approval.show;
        };

        $scope.nocomment = [];
        $scope.displayDocStatusMessage = function () {

        }
        $scope.approveApproval = function (approval) {
            console.log("approval", approval);
            if ($cordovaNetwork.isOffline()) {
                console.log("device offline");
                $rootScope.showToast($rootScope.getDisplayMessage(501));
                return;
            }
            if (!approval.comment) {
                approval.comment = "";
            }

            var doc_title = approval.Title;
            var st = true;
            $scope.showLoading("Loading...");
            GetServiceData.Approve(approval.TaskId, true, approval.comment).success(function (data, status, headers, config) {
                //$scope.hideLoading(); 

                GetServiceData.getData($scope.logindata.Login).success(function (data, status, headers, config) {

                    $scope.prepareApprovalsData(data);
                    $scope.hideLoading();
                    $scope.displayApprovalMsg(doc_title, st);

                }).error(function (data, status, headers, config) {

                    console.log("Error" + JSON.stringify(data));
                    $scope.hideLoading();
                });


            }).error(function () {
                $scope.hideLoading();
            });

        }

        $scope.rejectApproval = function (approval) {

            console.log("comment ", approval.comment);
            if ($cordovaNetwork.isOffline()) {
                console.log("device offline");
                $rootScope.showToast($rootScope.getDisplayMessage(501));
                return;
            }
            if (!approval.comment) {
                approval.comment = "";

            }
            var doc_title = approval.Title;
            var st = false;
            $scope.displayApprovalMsg(doc_title, st);
            $scope.showLoading("Loading...");

            GetServiceData.Approve(approval.TaskId, false, approval.comment).success(function (data, status, headers, config) {


                GetServiceData.getData($scope.logindata.Login).success(function (data, status, headers, config) {

                    $scope.hideLoading();
                    $scope.prepareApprovalsData(data);
                    $scope.displayApprovalMsg(doc_title, st);

                }).error(function (data, status, headers, config) {
                    $scope.hideLoading();


                });


            }).error(function () {
                $scope.hideLoading();
            })

        }
        $scope.scrollTop = function () {
            $ionicScrollDelegate.scrollTop();
        };
        $scope.displayApprovalMsg = function (doc_title, status) {
            console.log("doc_title" + doc_title + " status" + status);
            $scope.scrollTop();
            $scope.app_rej_visible = true;
            $scope.app_rej_msg = doc_title + " has been " + ((status) ? "approved" : "rejected");
            $timeout(function () {
                $scope.app_rej_visible = false;
            }, 4000);

        }
        $scope.setApprovalStatusClass = function (approval) {

            if (approval.Meeting) {
                if (approval.Meeting.SubmissionDeadlinePassed != undefined && approval.Meeting.SubmissionDeadlinePassed) {
                    return "red";

                } else if (approval.Meeting.SubmissionReminderPassed != undefined && approval.Meeting.SubmissionReminderPassed) {
                    return "yellow";
                } else {
                    return "green";

                }
            } else {
                return "green";
            }




        }

        /*$scope.showDocs = function(docs,$event){
         $event.stopPropagation();
         docs.is_open = !docs.is_open;
         }*/

        /*$scope.showDocs = function(docs,$event){
         $event.stopPropagation();
         
         /*var tmplt = "";
         for(doc in docs){
         
         tmplt += "<a ng-click=downloadFile(\""+encodeURIComponent(docs[doc].Url)+"\")>";
         tmplt += docs[doc].FileName;
         tmplt += "</a>";
         }*//*
          $scope.myDocs = docs;
          $scope.popup = $ionicPopup.alert({
          templateUrl: 'templates/popup.html',
          scope:$scope,
          okText:'Close',
          cssClass:'doc_popup'
          });
          }*/

        $scope.showDocs = function (docs, $event) {
            $event.stopPropagation();
            $scope.attachDocs = docs;
            $scope.openModal();
        }

        $scope.openModal = function () {
            if (!$scope.modal) {
                $ionicModal.fromTemplateUrl('templates/popup.html', {
                    scope: $scope,
                    animation: 'slide-in-up'
                }).then(function (modal) {
                    $scope.modal = modal;
                    $scope.modal.show();
                });
            } else {
                $scope.modal.show();
            }
        };

        $scope.closeModal = function () {
            $scope.modal.remove();
            $scope.modal = null;
        };

        $scope.$on('$destroy', function () {
            $scope.modal.remove();
            $scope.modal = null;
        });

        $scope.$on('modal.hidden', function () {
            console.log("modal hide");
            $scope.modal.remove();
            $scope.modal = null;
        });

        $scope.$on('modal.removed', function () {
            console.log("modal removed");
        });
        $scope.$on('modal.shown', function () {
            console.log('Modal is shown!');
        });



        $scope.showDocList = function (doc) {
            return doc.is_open;
        }
        /*
         $scope.showComment = function(approval_comment){
         console.log("approval_comment",approval_comment.show_comment);
         return approval_comment.show_comment;
         }*/
        $scope.toggleComment = function (approval_comment) {
            approval_comment.show_comment = !approval_comment.show_comment;
        }
        $scope.logindata = JSON.parse($rootScope.LocalStore.getItem("escribe"));
        console.log("Logindata", $scope.logindata);
        function findElement(arr, propName, propValue) {
            var res_arr = [];
            for (var i = 0; i < arr.length; i++) {
                if (arr[i][propName] == propValue)
                    res_arr.push(arr[i]);
            }
            // will return undefined if not found; you could return a default instead
            return res_arr;
        }
        function getFileNameFromUrl(url) {

            var split_url = url.split("/");

            var sup_doc_name = split_url[split_url.length - 1];
            console.log("sup_doc_name", sup_doc_name);
            return sup_doc_name;

        }

        $scope.removeFileExtension = function (file_name) {
            return file_name.substring(0, file_name.lastIndexOf("."));
        }


        $scope.prepareApprovalsData = function (approval_response) {
            $scope.approvals = approval_response.Approvals;

            if ($scope.approvals && $scope.approvals.length == 0) {
                $scope.no_approval_visible = true;
            }
            if ($scope.approvals.length > 0) {
                $scope.no_approval_visible = false;
                $rootScope.show_order_button = true;
                for (var approval in $scope.approvals) {
                    if (!$scope.approvals[approval]['Meeting']) {
                        $scope.approvals[approval]['Meeting'] = {};
                    } else {
                        var expr = /\/Date\((.*?)\)\//gi;
                        var stamp = expr.exec($scope.approvals[approval]['Meeting'].Date);
                        var timestamp = stamp[1].split("-", 1);
                        $scope.approvals[approval]['Meeting'].Date = timestamp[0];
                    }
                    if ($scope.approvals[approval]['SupportingDocuments'] && $scope.approvals[approval]['SupportingDocuments'].length > 0) {
                        for (var docs in $scope.approvals[approval]['SupportingDocuments']) {
                            var fileName = $scope.approvals[approval]['SupportingDocuments'][docs]['FileName'];
                            $scope.approvals[approval]['SupportingDocuments'][docs]['FileNameNoExt'] = $scope.removeFileExtension(fileName);
                        }
                    }
                    $scope.approvals[approval]['TitleNoExt'] = $scope.removeFileExtension($scope.approvals[approval]['Title']);
                    $scope.approvals[approval]['comment'] = "";
                    $scope.approvals[approval]['show_comment'] = false;
                }
            } else {
                $rootScope.show_order_button = false;
            }
            //console.log("$scope.approvals"+JSON.stringify($scope.approvals));

            $scope.resetSortBy();
        }
        $scope.toggle_approval_title = true;
        $scope.toggle_meeting_type = true;
        $scope.toggle_meeting_date = true;
        $scope.resetSortBy = function () {
            var ldata = getLocalstoreKey($rootScope);

            //console.log("Reset sort by ldata",ldata);
            if (ldata.Sort_criteria != "")
            {
                switch (ldata.Sort_criteria) {
                    case 'approval_title':
                        //$scope.toggle_approval_title = !$scope.toggle_approval_title;

                        $scope.temp_data = orderObjectByFilter($scope.approvals, 'Title', ldata.Order_by);
                        $scope.approvals = $scope.temp_data;

                        break;

                    case 'meeting_type':
                        //$scope.toggle_meeting_type = !$scope.toggle_meeting_type;
                        $scope.temp_data = orderObjectByFilter($scope.approvals, 'MeetingType.Title', ldata.Order_by);

                        $scope.approvals = $scope.temp_data;
                        break;

                    case 'meeting_date':
                        // $scope.toggle_meeting_date = !$scope.toggle_meeting_date;
                        $scope.temp_data = orderObjectByFilter($scope.approvals, 'Meeting.Date', ldata.Order_by);
                        $scope.approvals = $scope.temp_data;

                }
            }
        }
        $scope.sortBy = function (what) {
            var ldata = getLocalstoreKey($rootScope);

            //console.log("ldata",ldata);

            ldata["Sort_criteria"] = what;

            switch (what) {
                case 'approval_title':
                    $scope.toggle_approval_title = !$scope.toggle_approval_title;
                    ldata["Order_by"] = $scope.toggle_approval_title;
                    $scope.temp_data = orderObjectByFilter($scope.approvals, 'Title', $scope.toggle_approval_title);
                    $scope.approvals = $scope.temp_data;

                    break;

                case 'meeting_type':
                    $scope.toggle_meeting_type = !$scope.toggle_meeting_type;
                    ldata["Order_by"] = $scope.toggle_meeting_type;
                    $scope.temp_data = orderObjectByFilter($scope.approvals, 'MeetingType.Title', $scope.toggle_meeting_type);

                    $scope.approvals = $scope.temp_data;
                    break;

                case 'meeting_date':
                    $scope.toggle_meeting_date = !$scope.toggle_meeting_date;
                    ldata["Order_by"] = $scope.toggle_meeting_date;
                    $scope.temp_data = orderObjectByFilter($scope.approvals, 'Meeting.Date', $scope.toggle_meeting_date);
                    $scope.approvals = $scope.temp_data;

            }
            setLocalstoreKey($rootScope, "escribe", ldata);
            $rootScope.show_order_menu = false;
        }
        $scope.showLoading = function (str_label) {

            if (!str_label || str_label == "") {
                str_label = "Loading approvals...";
            }
            $ionicLoading.show({
                template: str_label
            }).then(function () {
                //console.log("The loading indicator is now displayed");
            });
        };
        $scope.hideLoading = function () {
            $ionicLoading.hide();
            /*$ionicLoading.hide().then(function(){
             
             });*/
        };
        /*if ($cordovaNetwork.isOffline()) {
            console.log("device offline");
            $rootScope.showToast($rootScope.getDisplayMessage(501));
        } else {*/
            $scope.showLoading();
            GetServiceData.getData($scope.logindata.Login).success(function (data, status, headers, config) {

                console.log("data", JSON.stringify(data));
                $scope.hideLoading();
                $scope.prepareApprovalsData(data);

            }).error(function (data, status, headers, config) {
                $scope.hideLoading();
                console.log("Error" + JSON.stringify(data));

            });
        //}




        $scope.doRefresh = function () {

            GetServiceData.getData($scope.logindata.Login).success(function (data, status, headers, config) {

                $scope.prepareApprovalsData(data);
                $scope.$broadcast('scroll.refreshComplete');


            }).error(function (data, status, headers, config) {
                console.log("Error" + JSON.stringify(data));
                $scope.$broadcast('scroll.refreshComplete');
            });
        }

        $scope.inAppOpen = function (doc_url) {
            try {
                window.open(doc_url, '_system');
            } catch (err) {
                console.log(err);
            }
        }

    $scope.downloadFile = function(doc_url,file_name,$event){
          console.log("in download function");
          $event.stopPropagation();
          document.addEventListener('deviceready', function () {
          console.log("url " +doc_url);
          console.log("cordova "+JSON.stringify(cordova));
          var folder = cordova.file.cacheDirectory;
          console.log("platform "+ionic.Platform.isAndroid());
          if(ionic.Platform.isAndroid())
          {
              folder = cordova.file.externalDataDirectory;
          }
          var targetPath =  folder + file_name;
          console.log("target path " + targetPath);
          var trustHosts = true;
          var logindata = LocalStoreData.getLocalData(); 

          var options = {
              headers : {
                  'Content-Type': 'application/json; charset=utf-8;',
                  'auth-token':logindata.Login.auth_token
              }
          };
          var mimes = {
              'pdf':'application/pdf', 
              'doc':'application/msword', 
              'docx':'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
              'xls':'application/vnd.ms-excel',
              'xlsx':'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
              'ppt':'application/powerpoint',
              'pptx':'application/vnd.openxmlformats-officedocument.presentationml.presentation',
              'bmp':'image/bmp',
              'gif':'image/gif',
              'jpeg':'image/jpeg',
              'jpg':'image/jpeg',
              'png':'image/png',
              'txt':'text/plain',
              'text':'text/plain',
              'word':'application/msword'
          };
          $scope.showLoading("downloading file...");
              $cordovaFileTransfer.download(doc_url, targetPath, options, trustHosts)
             .then(function(result) {
               console.log("+++++++++++++++ success "+ JSON.stringify(result));
               //console.log("+++++++++++++++ success "+ JSON.stringify(result.filesystem));
               console.log("Type = ",mimes[file_name.substring(file_name.lastIndexOf(".")+1).toLowerCase()]);
               $scope.hideLoading();
               console.log("file extension "+file_name.substring(file_name.lastIndexOf(".")+1).toLowerCase());
                  $cordovaFileOpener2.open(
                    
                    targetPath,
                      mimes[file_name.substring(file_name.lastIndexOf(".")+1).toLowerCase()],
                      {
                          error:function(){
                                console.log("in Error fileopener");
                          },
                          success:function(){
                                console.log("In success fileopener");
                          }
                      }
                    )
                    /*.then(function() {
                        console.log("file opened")
                    }, function(err) {
                        console.log("-- error "+ JSON.stringify(err));
                    });*/ 
             }, function(err) {
               console.log("----------  error "+ JSON.stringify(err));
               $scope.hideLoading();
             }, function (progress) {
               $timeout(function () {
                 downloadProgress = (progress.loaded / progress.total) * 100;
                 console.log(downloadProgress);
               });
             });
         });

     }
      $scope.removeFileExtension = function(file_name){
        return file_name.substring(0,file_name.lastIndexOf("."));
    }
}]);
