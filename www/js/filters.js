angular.module('starter.filters', []).filter('orderObjectBy', function () {
    return function (items, field, reverse) {

        console.log("filter call", field);

        var filtered = [];
        angular.forEach(items, function (item) {
            filtered.push(item);
        });
        filtered.sort(function (a, b) {

            if (field.indexOf(".") != -1) {
                var slipt_str = field.split(".");
                //console.log("slipt_str ",slipt_str);

                if (slipt_str[0] == "Meeting" && slipt_str[1] == "Date") {

                    var a_v = (a[slipt_str[0]][slipt_str[1]] != undefined ? a[slipt_str[0]][slipt_str[1]] : "0_" + a.Title.toLowerCase());
                    var b_v = (b[slipt_str[0]][slipt_str[1]] != undefined ? b[slipt_str[0]][slipt_str[1]] : "0_" + b.Title.toLowerCase());

                    return(a_v > b_v ? 1 : -1);
                } else {
                    var a_v = a[slipt_str[0]][slipt_str[1]];
                    var b_v = b[slipt_str[0]][slipt_str[1]];
                    if (typeof a_v == 'string') {
                        a_v = a_v.toLowerCase() + "_" + a.Title.toLowerCase();
                    }
                    if (typeof b_v == 'string') {
                        b_v = b_v.toLowerCase() + "_" + b.Title.toLowerCase();
                    }
                    if (a[slipt_str[0]][slipt_str[1]] == undefined)
                    {
                        a_v = "0_" + a.Title.toLowerCase();
                    }
                    if (b[slipt_str[0]][slipt_str[1]] == undefined) {
                        b_v = "0_" + b.Title.toLowerCase();
                    }
                    //return(a[slipt_str[0]][slipt_str[1]] > b[slipt_str[0]][slipt_str[1]] ? 1 : -1);
                    //console.log("(a_v > b_v ? 1 : -1) = ",(a_v > b_v ? 1 : -1));

                    return(a_v > b_v ? 1 : -1);


                }
            } else {
                var a_v = a[field];
                var b_v = b[field];
                if (typeof a_v == 'string') {
                    a_v = a_v.toLowerCase();
                }
                if (typeof b_v == 'string') {
                    b_v = b_v.toLowerCase();
                }
                return(a_v > b_v ? 1 : -1);
            }
            //return (a[field] > b[field] ? 1 : -1);


        });

        if (reverse)
            filtered.reverse();

        return filtered;
    };
})