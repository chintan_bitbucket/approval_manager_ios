// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'ngCordova', 'starter.controllers', 'starter.services', 'starter.filters'])

        .run(function ($ionicPlatform, $rootScope) {
            $ionicPlatform.ready(function () {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
   
  if (window.cordova && window.cordova.plugins.Keyboard) {
      console.log("In cordova plugin keyboard");
    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
    cordova.plugins.Keyboard.disableScroll(true);

  }
  if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      console.log("In statusbar");
      StatusBar.styleDefault();
      StatusBar.overlaysWebView(false);
      
    }
  });
  console.log("$rootScope.LocalStore",$rootScope.LocalStore);
  if(!$rootScope.LocalStore){
        $rootScope.LocalStore  = window.localStorage;
        console.log("$rootScope.LocalStore",$rootScope.LocalStore);  
    }
          
})

        .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })
  .state('login', {
    url: '/app/login',
    templateUrl: 'templates/login.html',
    controller: 'LoginCtrl'
  })

  /*.state('app.login', {
    url: '/login',
    views:{
        "loginContent":{
            templateUrl: 'templates/login.html',
            controller: 'LoginCtrl'
        }
    }
  })*/
  .state('app.approvals', {
    url: '/approvals',
    views: {
      'menuContent': {
        templateUrl: 'templates/approvals.html',
        controller: 'ApprovalsCtrl'
        
      }
    }
  })
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('app/login');
  $ionicConfigProvider.views.maxCache(0);
  
        }).controller("MainCtrl", ['$scope', '$rootScope', 'LoginService', '$state', '$ionicLoading', '$cordovaToast',
    function ($scope, $rootScope, LoginService, $state, $ionicLoading, $cordovaToast) {
      console.log("Call MainCtrl");
        if ($rootScope.LocalStore) {
        
        $rootScope.LocalDataEscribe = $rootScope.LocalStore.getItem('escribe');
     }
    //$rootScope.LocalStore.removeItem('Login');
        $rootScope.formdata = {portal_url: "", user_name: "", password: "", is_remember: false, login_method: "", auth_token: ""};
        if ($rootScope.LocalDataEscribe == undefined) {
            $rootScope.LocalStore.setItem('escribe', "{}");
        } else {
       var logindata = JSON.parse($rootScope.LocalDataEscribe);
            $scope.showLoading = function () {
        $ionicLoading.show({
          template: 'Loading...'
                }).then(function () {
           //console.log("The loading indicator is now displayed");
        });
    };
            $scope.hideLoading = function () {
       $ionicLoading.hide();
        /*$ionicLoading.hide().then(function(){

        });*/
    };
  
    // Auto login commented out.
    
            if (logindata.Login != undefined && logindata.Login.is_remember == true) {
            $rootScope.formdata.login_method = logindata.Login.login_method;
                $rootScope.formdata.user_name = logindata.Login.user_name;
            $rootScope.formdata.portal_url = logindata.Login.portal_url;
            $rootScope.formdata.is_remember = logindata.Login.is_remember;
            $rootScope.formdata.password = "";
    
           /*
                LoginService.loginUser(logindata.Login).success(function(data) {
                    console.log("Login Data",data);
                    //$scope.hideLoading();
                    if(data){
                        //var logindata = {portal_url:$scope.formdata.portal_url,user_name:$scope.formdata.user_name,is_remember:$scope.formdata.is_remember};
                        //$rootScope.LocalStore.setItem("escribe",JSON.stringify({"Login":logindata,"loginhash":""}));
                        $state.go('app.approvals');
                    }
                    else{
                        $scope.login_failed_msg = "Invalid login details.";
                        $rootScope.LocalStore.setItem("escribe","{}");
                    }
                }).error(function(data) {
                    $scope.login_failed_msg = "Invalid login details.";
                });
                  */
        }
        }
        
        $rootScope.showToast = function (msg) {
            $cordovaToast.showWithOptions({
                message: msg,
                duration: 4000,
                position: "center",
                addPixelsY: 0  // added a value to move it down a bit (default 0)
            });
        }
    
        
        $rootScope.getDisplayMessage = function (code) {
            switch (code) {
                /*  error codes 1 to 500
                 toast codes 501 to 1000
                 other codes 1001 to ~
                 add cases as per need in appropriate section
                 Please add appropriate comments for purpose of code */
    
                //  Error section
                case 1: //fileTransfer plugin error
                    return "File not found";
                    break;
                case 2: //fileTransfer plugin error
                    return "Invalid Url";
                    break;
                case 3: //fileTransfer plugin error
                    return "Internet connection error";
                    break;
                case 4: //fileTransfer plugin error
                    return "download aborted";
                    break;
                case 5: //fileTransfer plugin error
                    return "unknown error";
                    break;
    
     
    
    
                    //  Toast section
    
                case 501:  //cordovaNetwork plugin 
                    return "No internet";
                    break;


                case 506: //fileOpener2 plugin
                    return "Supporting App not found";
                    break;

                case 404:
                    return "File not found";
                    break;
                    
                case 401:
                    return "Authentication Required";
                    break;
                
                case 403:
                    return "Authentication Failed";
                    break;

                case 500:
                    return "Unable to download file";
                    break;

                case 550:
                    return "Unable to write file";
                    break;


                    //  default  ---> don't rely on this
                default:
                    return "error";
            }
        }

}]);
function getLocalstoreKey($rootScope, key) {
    if (!key) {
               key = "escribe";
          }
          var getlocaldata = $rootScope.LocalStore.getItem(key);
          return JSON.parse(getlocaldata);
}
function setLocalstoreKey($rootScope, key, val) {
    if (key && val) {
          val = JSON.stringify(val);
        $rootScope.LocalStore.setItem(key, val);
          }
}