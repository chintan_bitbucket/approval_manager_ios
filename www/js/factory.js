var service_app = angular.module('starter.services', []);
service_app.factory('PhonemanagerService', function ($http, $rootScope) {

    var factory = {};
    var api_url = "";
    var document_url = "";
    var login_localstore = JSON.parse($rootScope.LocalStore.getItem("escribe"));
    console.log("login_localstore", login_localstore);
    if (login_localstore != null || login_localstore != undefined) {
        if (login_localstore.Login != null || login_localstore.Login != undefined) {
            var logindata = login_localstore.Login;
            console.log("logindata", logindata);
            //        api_url = "https://"+logindata.portal_url+"/_vti_bin/eSCRIBE/ManagerMobile/service.svc";
            api_url = "https://" + logindata.portal_url + "/_vti_bin/eSCRIBE/Services/ManagerMobile";
        }
    }

    var config = {
        headers: {
            'Content-Type': 'application/json; charset=utf-8;'
        }
    };
    factory.getLocalData = function () {


        return JSON.parse($rootScope.LocalStore.getItem("escribe"));
    }
    factory.getItems = function (logindata) {
        //console.log("Get items logindata",logindata);
        var hdrs = {
            headers: {
                'Content-Type': 'application/json; charset=utf-8;',
                'auth-token': logindata.auth_token
            }
        }
        console.log("Get approvals" + api_url);
        var res = $http.post(api_url + "/Items", "", hdrs);
        return res;

    }
    factory.doLogin = function (logindata) {
        //console.log("Login data",logindata);
        // api_url = "https://"+logindata.portal_url+"/_vti_bin/eSCRIBE/ManagerMobile/service.svc";
        api_url = "https://" + logindata.portal_url + "/_vti_bin/eSCRIBE/Services/ManagerMobile";
        // var login_url = "https://"+logindata.portal_url+"/_vti_bin/eSCRIBE/Services/Authentication.svc/Login";
        var login_url = "https://" + logindata.portal_url + "/_vti_bin/eSCRIBE/Services/Authentication/Login";
        document_url = "https://" + logindata.portal_url + "/_vti_bin/eSCRIBE/Services/Documents/File";
        var res = $http.post(login_url + "/" + logindata.login_method, {
            "UserName": logindata.user_name,
            "Password": logindata.password
        }, config);
        return res;

    }

    factory.Approve = function (taskid, approved, comment) {
        var login = JSON.parse($rootScope.LocalStore.getItem("escribe"));

        var logindata = login.Login;
        var data = {
            "TaskId": taskid,
            "Approved": approved,
            "Comment": comment
        };
        var hdrs = {
            headers: {
                'Content-Type': 'application/json; charset=utf-8;',
                'auth-token': logindata.auth_token
            }
        }
        //var res = $http.post(api_url+"/ApproveWithAuthentication",data,config);
        var res = $http.post(api_url + "/Approve", data, hdrs);

        return res;


    }
    factory.getDocument = function (doc_url) {
        var login = JSON.parse($rootScope.LocalStore.getItem("escribe"));
        var logindata = login.Login;
        var config = {
            params: {
                Url: doc_url
            },
            headers: {
                'auth-token': logindata.auth_token
            }
        }
        console.log("Config = ",JSON.stringify(config));
        
        console.log("document_url = "+document_url);

        var res = $http.get(document_url, config);
        console.log("+++ res --- "+JSON.stringify(res));
        return res;
    }

    return factory;


});
service_app.service('GetServiceData', function (PhonemanagerService) {

    this.getData = function (logindata) {
        return PhonemanagerService.getItems(logindata);
        // return PhonemanagerService.getItems();
    }
    this.Approve = function (taskid, approved, comment) {
        return  PhonemanagerService.Approve(taskid, approved, comment);
    }
    this.getdocument = function (doc_url) {
        return  PhonemanagerService.getDocument(doc_url);
    }
});
service_app.service('LoginService', function ($q, PhonemanagerService) {


    return {
        loginUser: function (logindata) {
            var deferred = $q.defer();
            var promise = deferred.promise;

            return PhonemanagerService.doLogin(logindata);

        }



    }
});
service_app.service('LocalStoreData', function ($q, PhonemanagerService) {
    return {
        getLocalData: function () {
            return PhonemanagerService.getLocalData();
        }
    }
});
